#include "ast.hh"
#include <cstdio>
#include <iostream>

using Utility::String_Pool;

extern int yylineno;
extern int yycolno;

namespace Turtle {

    Token::~Token() {
        /* compulsory, pure destructor to make this abstract base class */
    }

    Binary_Expression::~Binary_Expression() {
        delete left;
        delete right;
    }

    Unary_Expression::~Unary_Expression() {
        delete exp;
    }

    Binding::~Binding() {
        delete id;
        delete exp;
    }

    Bindings::~Bindings() {
        for (auto i : list)  {
            delete i;
        }
    }

    Parameters::~Parameters() {
        for (auto i : list) {
            delete i;
        }
    }

    Function_Declaration::~Function_Declaration() {
        delete compound_statement;
        delete bindings;
        delete parameters;
        delete id;
    }

    Function_List::~Function_List() {
        for (auto i : list) {
            delete i;
        }
    }

    Move_Statement::~Move_Statement() {
        delete right;
        delete left;
    }

    Read_Statement::~Read_Statement() {
        delete id;
    }

    Assign_Statement::~Assign_Statement() {
        delete exp;
        delete id;
    }

    Comparison::~Comparison() {
        delete right;
        delete left;
    }

    If_Statement::~If_Statement() {
        delete else_clause;
        delete if_clause;
        delete comp;
    }

    While_Statement::~While_Statement() {
        delete body;
        delete cond;
    }
    Return_Statement::~Return_Statement() {
        delete exp;
    }


    Compound_Statement::~Compound_Statement() {
        for (auto i : list) {
            delete i;
        }
    }

    Program::~Program() {
        delete body;
        delete function_list;
        delete bindings;
        delete label;
    }

    Expression_List::~Expression_List() {
        for (auto i : list) {
            delete i;
        }
    }

    Function_Call_Expression::~Function_Call_Expression() {
        delete id;
        delete list;
    }

    void Program::print(std::ostream &out) {
        out << label << std::endl;
    }
    Function_Call_Statement::~Function_Call_Statement() {
        delete exp;
    }
}
