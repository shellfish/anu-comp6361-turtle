#ifndef COMMON_H
#define COMMON_H

#include "ast.hh"
typedef const Turtle::Token* YYSTYPE;
#define YYSTYPE YYSTYPE

void yyerror(const char *s);
extern const Turtle::Program *syn_tree;

namespace Turtle {
    void generate(const Program * prog, const char flag);
}

#endif
