%option noyywrap nodefault yylineno
%x COMMENT
%{
#include "common.hh"
#include "turtle_syn.hh"
#include "utility.hh"
#include <stdlib.h>

static Utility::String_Pool string_pool;
int yycolno = 0;
%}

%%

"+" |
"-" |
"*" |
"=" |
"(" |
")" |
"," |
"{" |
"}" |
"<" { yycolno++; return yytext[0]; }

([1-9][0-9]*|0) { yycolno += strlen(yytext);
                  yylval = new Turtle::Number(atoi(yytext), yylineno, yycolno);
                  return NUMBER; }

turtle { yycolno += 6; return TURTLE; }
var { yycolno += 3; return VAR;}
fun { yycolno += 3; return FUN; }
up { yycolno += 2; return UP; }
down { yycolno += 4; return DOWN;}
moveto { yycolno += 6; return MOVETO; }
read { yycolno += 4; return READ; }
if { yycolno += 2; return IF;}
else { yycolno +=4; return ELSE; }
while { yycolno += 5; return WHILE; }
return { yycolno += 6; yylval = new Turtle::Identifier(
        string_pool.ref(yytext), yylineno, yycolno);
        return RETURN; }
== { yycolno += 2; return EQ; }

[a-zA-Z][[:alnum:]_']* { yycolno += strlen(yytext);
                         yylval = new Turtle::Identifier(
                           string_pool.ref(yytext), yylineno, yycolno);
                        return ID; }

"//"  { yycolno += 2; BEGIN COMMENT; }
<COMMENT>. { yycolno++; }
<COMMENT>\n {  BEGIN INITIAL; }
<COMMENT><<EOF>>  { yyerror("Unterminated comment"); }

[ \t] { yycolno++; }

. { yycolno++;
    char fmtbuf[] = "illegal character: %2d";
    char tmpbuf[sizeof(fmtbuf)];
    sprintf(tmpbuf, fmtbuf, yytext[0]);
    yyerror(tmpbuf); }

\n { yycolno = 0; }

%%
