#ifndef AST_H
#define AST_H

#include "utility.hh"
#include <string>
#include <vector>
#include <ostream>
#include <iostream>

namespace Turtle {
    class Token;
}

namespace { // unnamed namespace
    template<class T>
    const T* dn(const Turtle::Token* t) {
        // down casting
        return dynamic_cast<T*>(const_cast<Turtle::Token*>(t));
    }
}

namespace Turtle {

    class Token {
    public:
         virtual ~Token() = 0;
    };

    class Nonterminal : public Token {};

    // Terminal symbols convey the position information
    class Terminal : public Token {
    public:
        const int lineno;
        const int colno;
        Terminal(int lineno, int colno) : lineno(lineno), colno(colno) {}
        virtual std::string to_string() const {
            return "(" + std::to_string(lineno) + ":" + std::to_string(colno) + ")"; }
    };

    class Identifier : public Terminal {
    public:
        const std::string &name;
        Identifier(const std::string &name, int lineno, int colno)
            : Terminal(lineno, colno), name(name) {}
        virtual std::string to_string() const override {
            return name + Terminal::to_string();
        }
    };

    class Number : public Terminal {
    public:
        const int value;
        Number(int value, int lineno, int colno)
            : Terminal(lineno, colno), value(value) {}
    };

    enum Operator {
        OP_PLUS,
        OP_MINUS,
        OP_TIMES,
        OP_UMINUS,
    };

    class Expression : public Nonterminal {
    public:
        enum class Type {
            Literal, Variable, Funccall, Binary, Unary
        };

        virtual Type type() const = 0;
    };

    class Expression_List : public Nonterminal {
    private:
        std::vector<const Expression*> list;
    public:
        typedef std::vector<const Expression*>::const_iterator const_iterator;
        typedef std::vector<const Expression*>::size_type size_type;
        size_type size() const {  return list.size(); }
        const_iterator cbegin() const { return list.cbegin(); }
        const_iterator cend() const { return list.cend(); }

        const Expression_List *add(const Expression *exp) {
            list.push_back(exp);
            return this;
        }
        const Expression_List *add(const Token*exp) {
            return add(dn<Expression>(exp));
        }
        ~Expression_List();
    };

    class Function_Call_Expression : public Expression {
    public:
        Type  type() const override { return Type::Funccall; }
        const Identifier *id;
        const Expression_List* list;
        Function_Call_Expression(const Identifier *id, const Expression_List* list)
            : id(id), list(list) {}
        Function_Call_Expression(const Token*id, const Token *list)
            : Function_Call_Expression(dn<Identifier>(id), dn<Expression_List>(list)) {}
        ~Function_Call_Expression();
    };

    class Binary_Expression : public Expression {
    public:
        Type type() const override { return Type::Binary; }
        const Expression *left;
        const Operator op;
        const Expression *right;
        Binary_Expression(const Expression *left,
                         const Operator op,
                         const Expression *right)
            : left(left), op(op), right(right) {}
        Binary_Expression(const Token *left, const Operator op, const Token*right)
            : Binary_Expression(dn<Expression>(left), op, dn<Expression>(right)) {}
        ~Binary_Expression();
    };

    class Unary_Expression : public Expression {
    public:
        Type type() const override { return Type::Unary; }
        const Operator op;
        const Expression *exp;
        Unary_Expression(const Operator op, const Expression *exp)
            : op(op), exp(exp) {}
        Unary_Expression(const Operator op, const Token *exp)
            : op(op), exp(dn<Expression>(exp)) {}
        ~Unary_Expression();
    };

    class Number_Expression : public Expression {
    public:
        Type type() const override { return Type::Literal; }
        const Number *val;
        Number_Expression(const Number *val) : val(val) {}
        Number_Expression(const Token* val) : val(dn<Number>(val)) {}
        ~Number_Expression() { delete val; }
    };

    class Identifier_Expression : public Expression {
    public:
        Type type() const override { return Type::Variable; }
        const Identifier *id;
        Identifier_Expression(const Identifier *id) : id(id) {}
        Identifier_Expression(const Token *id) : id(dn<Identifier>(id)) {}
        ~Identifier_Expression() { delete id;}
    };

    class Binding : public Nonterminal {
    public:
        const Identifier *id;
        const Expression *exp;
        Binding(const Identifier *id, const Expression *exp)
            : id(id), exp(exp) {}
        Binding(const Token* t1, const Token *t2)
            : Binding(dn<Identifier>(t1), dn<Expression>(t2)) {}
        Binding(const Identifier *id) : id(id), exp(0) {}
        Binding(const Token* t1) : Binding(dn<Identifier>(t1)) {}
        ~Binding();
    };


    class Bindings : public Nonterminal {
    public:
        typedef std::vector<const Binding*> bindlist;
        typedef bindlist::const_iterator const_iterator;
        const_iterator cbegin() const { return list.cbegin(); }
        const_iterator cend() const { return list.cend(); }
        const Bindings * add(const Binding *binding)
          {list.push_back(binding);  return this; };
        const Bindings * add(const Token* t) { return add(dn<Binding>(t)); }
        ~Bindings();
    private:
        bindlist list;
    };

    class Parameters : public Nonterminal {
    private:
        std::vector<const Identifier*>list;
    public:
        typedef std::vector<const Identifier*>::size_type size_type;
        typedef std::vector<const Identifier*>::const_iterator const_iterator;
        const_iterator cbegin() const { return list.cbegin(); }
        const_iterator cend() const { return list.cend(); }
        size_type size() const { return list.size(); }

        const Parameters *add(const Token* t) {
            list.push_back(dn<Identifier>(t));
            return this;
        }

        ~Parameters();
    };

    class Compound_Statement;

    class Function_Declaration : public Nonterminal {
    public:
        const Identifier *id;
        const Parameters *parameters;
        const Bindings   *bindings;
        const Compound_Statement *compound_statement;
        Function_Declaration(const Identifier *id,
                             const Parameters *parameters,
                             const Bindings *bindings,
                             const Compound_Statement *compst)
            : id(id), parameters(parameters),
              bindings(bindings), compound_statement(compst) {}
        Function_Declaration(const Token* t1, const Token *t2,
                             const Token* t3, const Token *t4)
            : Function_Declaration(dn<Identifier>(t1), dn<Parameters>(t2),
                                   dn<Bindings>(t3), dn<Compound_Statement>(t4)) {}
        Parameters::size_type parameter_no() const {
            if (parameters) {
                return parameters->size();
            } else {
                return 0;
            }
        }
        ~Function_Declaration();
    };

    class Function_List : public Nonterminal {
    public:
        typedef std::vector<const Function_Declaration*> funclist;
        typedef funclist::iterator iterator;
        typedef funclist::const_iterator const_iterator;

        // call begin() on const object should return const_iterator
        const_iterator cbegin() const { return list.cbegin(); }
        const_iterator cend() const { return list.cend(); }

        const Function_List *add(const Function_Declaration* fd) {
            list.push_back(fd);
            return this;
        }
        const Function_List *add(const Token*fd) {
            return add(dn<Function_Declaration>(fd));
        }
        ~Function_List();
    private:
        funclist list;
    };

    class Statement : public Nonterminal {
    public:
        enum class Type {
            Up, Down, Call, Move, Read, Assign, If, While, Return, Compound
        };
        virtual Type type() const = 0;
    };

    class Up_Statement : public Statement {
        Type type() const override { return Type::Up; }
    };

    class Down_Statement : public Statement {
        Type type() const override { return Type::Down; }
    };

    class Function_Call_Statement : public Statement {
    public:
        Type type() const override { return Type::Call; }
        const Function_Call_Expression *exp;
        Function_Call_Statement(const Function_Call_Expression *exp) : exp(exp) {}
        Function_Call_Statement(const Token* exp):exp(dn<Function_Call_Expression>(exp)) {}
        ~Function_Call_Statement();
    };

    class Move_Statement : public Statement {
    public:
        Type type() const override { return Type::Move; }
        const Expression *left;
        const Expression *right;
        Move_Statement(const Expression*left, const Expression*right)
            : left(left), right(right) {}
        Move_Statement(const Token* left, const Token* right)
            : Move_Statement(dn<Expression>(left), dn<Expression>(right)) {}
        ~Move_Statement();
    };

    class Read_Statement : public Statement {
    public:
        Type type() const override { return Type::Read; }
        const Identifier *id;
        Read_Statement(const Identifier* id) : id(id) {}
        Read_Statement(const Token* t) : Read_Statement(dn<Identifier>(t)) {}
        ~Read_Statement();
    };

    class Assign_Statement : public Statement {
    public:
        Type type() const override { return Type::Assign; }
        const Identifier *id;
        const Expression *exp;
        Assign_Statement(const Identifier*id, const Expression *exp)
            : id(id), exp(exp) {}
        Assign_Statement(const Token*t1, const Token*t2)
            : Assign_Statement(dn<Identifier>(t1), dn<Expression>(t2)) {}
        ~Assign_Statement();
    };

    enum If_Operator {
        OP_EQ,
        OP_LT,
    };

    class Comparison : public Nonterminal {
    public:
        const Expression* left;
        const If_Operator op;
        const Expression* right;
        Comparison(const Expression *left,
                    const If_Operator op,
                    const Expression *right)
            : left(left), op(op), right(right) {}
        Comparison(const Token* left, const If_Operator op, const Token* right)
            : Comparison(dn<Expression>(left), op, dn<Expression>(right)) {}
        ~Comparison();
    };

    class If_Statement : public Statement {
    public:
        Type type() const override { return Type::If; }
        const Comparison *comp;
        const Statement *if_clause;
        const Statement * else_clause;
        If_Statement(const Comparison *comp,
                     const Statement* if_clause,
                     const Statement* else_clause)
            :  comp(comp), if_clause(if_clause), else_clause(else_clause) {}
        If_Statement(const Token* c, const Token* i, const Token* e)
            : If_Statement(dn<Comparison>(c), dn<Statement>(i), dn<Statement>(e))
        {}
        ~If_Statement();
    };


    class While_Statement : public Statement {
    public:
        Type type() const override { return Type::While; }
        const Comparison * cond;
        const Statement * body;
        While_Statement(const Comparison *cond, const Statement *body)
            : cond(cond), body(body) {}
        While_Statement(const Token* c, const Token* b)
            : While_Statement(dn<Comparison>(c), dn<Statement>(b)) {}
        ~While_Statement();
    };

    class Return_Statement : public Statement {
    public:
        Type type() const override { return Type::Return; }
        const Expression *exp;
        const int lineno;
        const int colno;
        Return_Statement(const Expression* exp, int lineno, int colno)
            : exp(exp), lineno(lineno), colno(colno) {}
        Return_Statement(const Token*t, int lineno, int colno)
            : Return_Statement(dn<Expression>(t), lineno, colno) {}
        ~Return_Statement();
    };


    class Compound_Statement : public Statement {
    private:
        std::vector<const Statement*> list;
    public:
        Type type() const override { return Type::Compound; }
        typedef std::vector<const Statement*>::const_iterator const_iterator;
        const_iterator cbegin() const { return list.cbegin(); }
        const_iterator cend() const { return list.cend(); }
        const Compound_Statement *add(const Token* s) {
            list.push_back(dn<Statement>(s));
            return this;
        }
        ~Compound_Statement();
    };

    class Program : public Nonterminal {
    public:
        int count = 0;
        const Identifier* label;
        const Bindings* bindings;
        const Function_List* function_list;
        const Compound_Statement* body;
        Program(const Identifier* label, const Bindings* bindings,
                const Function_List* function_list, const Compound_Statement *body)
            : label(label), bindings(bindings), function_list(function_list), body(body) {}
        Program(const Token* t1, const Token*t2, const Token* t3, const Token*t4)
            : Program(dn<Identifier>(t1), dn<Bindings>(t2),
                      dn<Function_List>(t3), dn<Compound_Statement>(t4)) {}
        ~Program();
        void print(std::ostream &out);
    };
}

#endif
