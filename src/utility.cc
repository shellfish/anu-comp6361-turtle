#include "utility.hh"
#include <stdexcept>
namespace Utility {

const std::string&
    String_Pool::ref(const std::string &key)
    {
        try {
            return this->pool.at(key);
        } catch (const std::out_of_range& e) {
            std::string& nonconkey = const_cast<std::string&> (key);
            this->pool.insert(
                std::make_pair<std::string&,std::string&>(nonconkey, nonconkey));
            return this->pool.at(key);
        }
    }

} // close namespace utility
