%token NUMBER
%token EQ "=="
%token ID "Identifier"
%token END 0 "end of file"

/* reserved words */
%token TURTLE VAR FUN UP DOWN MOVETO
%token READ IF ELSE WHILE RETURN
%left "+" "-"
%left "*"
%nonassoc  UMINUS

%{
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif
#include "common.hh"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <iostream>
#include <unistd.h>
using namespace Turtle;

const Program *syn_tree = 0;
int yylex(void);
extern int yylineno;
extern int yycolno;

%}

%%

prog : TURTLE ID bindings funclist compstat
       { syn_tree =  new Program($2, $3, $4, $5); $$ = 0;}
;

binding : VAR ID '=' exp {$$ = new Binding($2, $4); }
        | VAR ID {$$ = new Binding($2); }
;

bindings : bindings binding
            {$$ = dynamic_cast<Bindings*>(const_cast<Token*>($1))->add($2); }
         | binding  { $$ =  (new Bindings())->add($1); }
         | { $$ = 0; }
;

funcdecl : FUN ID '(' parameters ')' bindings compstat
         {
            $$ = new Function_Declaration($2, $4, $6, $7);
         }
;

funclist : funcdecl { $$ = (new Function_List())->add($1); }
         | funclist funcdecl
            { $$ = dynamic_cast<Function_List*>
                    (const_cast<Token*>($1))->add($2); }
         | { $$ = 0;}

parameters : parameters  ',' ID
                { $$ = dynamic_cast<Parameters*>(
                            const_cast<Token*>($$))->add($3); }
           | ID { $$ = (new Parameters())->add($1); }
           | { $$ = 0;}
;

stat : UP { $$ = new Up_Statement(); }
     | DOWN { $$ = new Down_Statement(); }
     | MOVETO '(' exp ',' exp ')' {$$ = new Move_Statement($3, $5); }
     | READ '(' ID ')' { $$ = new Read_Statement($3); }
     | ID '=' exp {$$ = new Assign_Statement($1, $3); }
     | ifstat
     | whileloop { $$ = $1; $1 = 0;}
     | RETURN exp {
        auto t = dynamic_cast<const Identifier*>($1);
        delete t;
        $$ = new Return_Statement($2, t->lineno, t->colno); }
     | compstat
     | exp {$$ = new Function_Call_Statement($1); }
;

ifstat : IF '(' comparison ')' stat { $$ = new If_Statement($3, $5, 0); }
       | IF '(' comparison ')' stat ELSE stat
            { $$ = new If_Statement($3, $5, $7); }
;

comparison : exp EQ  exp  {$$ = new Comparison($1, OP_EQ, $3); }
           | exp '<' exp  {$$ = new Comparison($1, OP_LT, $3); }
;

whileloop : WHILE '(' comparison ')' stat
          { $$ = new While_Statement($3, $5); }
;

compstat : '{' statlist '}' {$$ = $2;  }
;

statlist : stat {$$ = (new Compound_Statement())->add($1); }
         | statlist stat
            {$$ = dynamic_cast<Compound_Statement*>(
                     const_cast<Token*>($1))->add($2); }
         | { $$ = 0; }
;

explist : explist ',' exp {$$ = dynamic_cast<Expression_List*>(
                    const_cast<Token*>($1))->add($2); }
        | exp  {$$ = (new Expression_List())->add($1); }
        | {$$ = 0;}
;

exp : exp '+' exp  {$$ = new Binary_Expression($1, OP_PLUS, $3); }
    | exp '-' exp  {$$ = new Binary_Expression($1, OP_MINUS, $3); }
    | exp '*' exp  {$$ = new Binary_Expression($1, OP_TIMES, $3); }
    | '(' exp ')' { $$ = $2; $2 = 0;}
    | '-' exp %prec UMINUS { $$ = new Unary_Expression(OP_UMINUS, $2); }
    | NUMBER  { $$ = new Number_Expression($1); }
    | ID { $$ = new Identifier_Expression($1); }
    | ID '(' explist ')' {$$ = new Function_Call_Expression($1, $3);  }
;

%%

extern FILE *yyin;
std::string *input_file = 0;
const char *input_file_name;

int
main(int argc, char **argv)
{
    int opt;
    char flag = 'b';
    while ((opt = getopt(argc, argv, "Aado:i:")) != -1) {
        switch (opt) {
#ifdef YYDEBUG
        case 'd':
            yydebug = 1;
            flag = 'd';
            break;
#endif
        case 'i':
            input_file = new std::string(optarg);
            break;
        case 'a':
            flag = 'a';
            break;
        case 'A':
            flag = 'A';
            break;
        }
    }

    if (input_file) {
        yyin = fopen(input_file->c_str(), "r");
        if (!yyin) {
            fprintf(stderr, "error: %s\n", strerror(errno));
            return 1;
        }
        input_file_name = input_file->c_str();
    } else {
        yyin = stdin;
        input_file_name = "stdin";
    }

    yyparse();

    if (syn_tree && flag != 'd') {
        Turtle::generate(syn_tree, flag);
        delete syn_tree;
        return 0;
    } else {
        return 2;
    }
}

void
yyerror(const char *s)
{
    fprintf(stderr, "%s:%d:%d: %s\n", input_file_name, yylineno, yycolno, s);
}
