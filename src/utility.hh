#ifndef UTILITY_H
#define UTILITY_H

#include <unordered_map>
#include <string>

namespace Utility {
    class String_Pool {
        std::unordered_map<std::string, std::string> pool;
        public:
        const std::string& ref(const std::string &key);
    };
}

#endif
