#include "common.hh"
#include <unordered_map>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <typeinfo>
#include <ostream>
#include <stdint.h>

extern const char *input_file_name;


void
warning(int lineno, int colno, const std::string &msg)
{
    fprintf(stderr, "warning: %s:%d:%d: %s\n", input_file_name, lineno, colno, msg.c_str());
}

void
fatal(int lineno, int colno, const std::string &msg)
{
    fprintf(stderr, "%s:%d:%d: %s, ", input_file_name, lineno, colno, msg.c_str());
    fprintf(stderr, "fatal error... \n");
    exit(1);
}

void info(const std::string& msg) {
    //std::cerr << msg << std::endl;
}


namespace Turtle {

    class Environment { // new
    public:
        typedef const char*  Key;
        int find(Key key);
        bool add(Key key, int offset);
    private:
        std::unordered_map<Key, int> table;
    };

    int Environment::find(Environment::Key key) {
        try {
            return table.at(key);
        } catch (const std::out_of_range& e) {
            return -1;
        }
    }

    bool Environment::add(Environment::Key key, int offset) {
        if (find(key) != -1) {
           return false;
        } else {
            table[key] = offset;
            return true;
        }
    }

    class Block;
    class Context {
    public:
        typedef const char *  Entry;
        typedef std::unordered_map<Entry, Block*>::size_type size_type;
        size_type size() { return defs.size(); }
        typedef std::unordered_map<Entry, Block*>::iterator iterator;
        iterator begin()  { return defs.begin(); }
        iterator end() { return defs.end(); }
        void redecl(Entry e, int no) { // use as instruction offset instead of paramter number
            specs[e] = no;
        }
        int finddecl(Entry e);
        bool declare(Entry e, int no);
        bool define(Entry e, Block* body);
        Block *finddef(Entry e);
        bool match(const Function_Call_Expression* e);
    private:
        std::unordered_map<Entry, int> specs;
        std::unordered_map<Entry, Block*> defs;
    };

    // should test null which represents 0 length parameter list
    bool Context::match(const Function_Call_Expression* e) {
        int lno = e ? e->list->size() : 0;
        int rno = finddecl(e->id->name.c_str());
        return lno == rno;
    }

    int Context::finddecl(Entry e) {
        try {
            return specs.at(e);
        } catch (const std::out_of_range& x) {
            return -1;
        }
    }

    bool Context::declare(Entry e, int no) {
        if (finddecl(e) < 0) {
            specs[e] = no;
            return true;
        } else {
            return false;
        }
    }

    Block *Context::finddef(Entry e) {
        try {
            return defs.at(e);
        } catch (const std::out_of_range& x) {
            return 0;
        }
    }

    bool Context::define(Entry e, Block* body) {
        if (finddecl(e) < 0) {
            return false;
        } else {
            defs[e] = body;
            return true;
        }
    }

    namespace Instruction {
        enum class Type {
            Normal,Jump,Rjump
        };

        class Base {
        public:
            virtual int words() { return 1;} // default to single word instruction
            virtual void write_asm(std::ostream & out) = 0;
            virtual void write_bin(std::ostream& out) = 0;
            virtual Type type() { return Type::Normal; }
        };

        class Relocateable : public Base {
        protected:
            int absolute_dest = -1;
        public:
            int get_absolute_dest() { return absolute_dest; }
            void set_absolute_dest(int n ) { absolute_dest = n;}
        };

        class Halt : public Base {
        private:
        public:
            virtual void write_asm(std::ostream & out) override {
                out << "halt" << std::endl;
            }
            virtual void write_bin(std::ostream & out) override {
                uint16_t tmp = 0x0000;
                out << tmp << std::endl;
            }
        };

        class Return : public Base {
        public:
            virtual void write_asm(std::ostream & out) override {
                out << "return" << std::endl;
            }
            virtual void write_bin(std::ostream& out) override {
                uint16_t tmp = 0x2800;
                out << tmp << std::endl;
            }
        };

        class Addition : public Base {
        public:
            virtual void write_asm(std::ostream & out) override {
                out << "add" << std::endl;
            }

            virtual void write_bin(std::ostream &out) override {
                uint16_t tmp = 0x1000;
                out << tmp << std::endl;
            }
        };

        class Multiplication : public Base {
        public:
            virtual void write_asm(std::ostream & out) override {
                out << "multiply" << std::endl;
            }
            void write_bin(std::ostream &out) override {
                uint16_t tmp = 0x1400;
                out << tmp << std::endl;
            }
        };

        class Down : public Base {
        public:
            virtual void write_asm(std::ostream & out) override {
                out << "down"<< std::endl;
            }
            void write_bin(std::ostream &out) override {
                uint16_t tmp = 0x0c00;
                out << tmp << std::endl;
            }
        };

        class Negation : public Base {
        public:
            virtual void write_asm(std::ostream & out) override {
                out << "negate" << std::endl;
            }
            void write_bin(std::ostream &out) override {
                uint16_t tmp = 0x2200;
                out <<  tmp << std::endl;
            }
        };

        class Up : public Base {
        private:
        public:
            virtual void write_asm(std::ostream & out) override {
                out << "up" << std::endl;
            }
            void write_bin(std::ostream &out) override {
                uint16_t tmp = 0x0a00;
                out <<  tmp << std::endl;
            }
        };

        class Subtraction : public Base {
        public:
            virtual void write_asm(std::ostream & out) override {
                out << "subtract" << std::endl;
            }
            void write_bin(std::ostream &out) override {
                uint16_t tmp = 0x1200;
                out <<  tmp << std::endl;
            }
        };

        class Load_GP : public Base {
        public:
            const int offset;
            Load_GP(const int o) : offset(o) {}
            virtual void write_asm(std::ostream & out) override {
                out << "load\t" << offset << "(GP)"  << std::endl;
            }

            void write_bin(std::ostream &out) override {
                uint16_t p = 0x0600;
                uint8_t tmp = offset;
                out << (p ^ tmp) << std::endl;
            }
        };

        class Load_FP : public Base {
        public:
            const int offset;
            Load_FP(const int o) : offset(o) {}
            virtual void write_asm(std::ostream & out) override {
                out << "load\t" << offset << "(FP)"  << std::endl;
            }

            void write_bin(std::ostream &out) override {
                uint16_t p = 0x0700;
                uint8_t tmp = offset;
                out << (p ^ tmp) << std::endl;
            }
        };

        class Store_GP : public Base {
        public:
            const int offset;
            Store_GP(const int o) : offset(o) {}

            virtual void write_asm(std::ostream & out) override {
                out << "store\t" << offset << "(GP)"  << std::endl;
            }

            void write_bin(std::ostream &out) override {
                uint16_t p = 0x0400;
                uint8_t tmp = offset;
                out << (p ^ tmp) << std::endl;
            }
        };

        class Store_FP : public Base {
        public:
            const int offset;
            Store_FP(const int o) : offset(o) {}
            virtual void write_asm(std::ostream & out) override {
                out << "store\t" << offset << "(FP)"  << std::endl;
            }

            void write_bin(std::ostream &out) override {
                uint16_t p = 0x0500;
                uint8_t tmp = offset;
                out << (p ^ tmp) << std::endl;
            }
        };

        // jsr
        class Jump : public Relocateable {
        public:
            const char *entry;
            Jump(const char *entry) : entry(entry) {}
            virtual void write_asm(std::ostream & out) override {
                if (absolute_dest < 0) {
                    out << "jsr\t" << "\"" << entry << "\"" << std::endl;
                } else {
                    out << "jsr\t" << absolute_dest << std::endl;
                }
            }

            void write_bin(std::ostream &out) override {
                uint16_t tmp = 0x6800;
                out << tmp << std::endl;
                tmp = get_absolute_dest();
                out << tmp << std::endl;
            }

            Type type() override { return Type::Jump; }
            int words() override { return 2;}
        };

        class Pop: public Base {
        public:
            const int num;
            Pop(int num) : num(num) {}

            virtual void write_asm(std::ostream & out) override {
                out << "pop\t"  << num << std::endl;
            }

            virtual void write_bin(std::ostream &out) override {
                uint16_t tmp = 0x5e00;
                out << tmp << std::endl;
                tmp = num;
                out << tmp << std::endl;
            }
        };

        class Immediate : public Base {
        public:
            const int value;
            Immediate(int value) : value(value) {}

            virtual void write_asm(std::ostream & out) override {
                out << "load\t#" << value  << std::endl;
            }
            int words() override { return 2;}

            void write_bin(std::ostream &out) override {
                uint16_t tmp = 0x5600;
                out << tmp << std::endl;
                tmp = value;
                out << tmp << std::endl;
            }
        };

        class Rjump : public Relocateable {
        protected:
            int jump;
        public:
            int get() { return jump;}
            void  set(int n) { jump = n; }
            Type type() override { return Type::Rjump; }
            int words() override { return 2;}

        };

        class Relative_Jump : public Rjump {
        public:
            virtual void write_asm(std::ostream & out) override {
                if (absolute_dest < 0) {
                    out << "rjump\t" << "[" << jump << "]" << std::endl;
                } else {
                    out << "jump\t" << absolute_dest << std::endl;
                }
            }

            void write_bin(std::ostream &out) override {
                uint16_t tmp = 0x7000;
                out << tmp << std::endl;
                tmp = get_absolute_dest();
                out << tmp << std::endl;
            }
        };

        class Jeq : public Rjump {
        public:
            virtual void write_asm(std::ostream & out) override {
                if (absolute_dest < 0) {
                    out << "jeq\t" << "[" << jump << "]" << std::endl;
                } else {
                    out << "jeq\t" << absolute_dest << std::endl;
                }
            }

            void write_bin(std::ostream &out) override {
                uint16_t tmp = 0x7200;
                out << tmp << std::endl;
                tmp = get_absolute_dest();
                out << tmp << std::endl;
            }
        };

        class Jlt : public Rjump {
        public:
            virtual void write_asm(std::ostream & out) override {
                if (absolute_dest < 0) {
                    out << "jlt\t" << "[" << jump << "]" << std::endl;
                } else {
                    out << "jlt\t" << absolute_dest << std::endl;
                }
            }

            void write_bin(std::ostream &out) override {
                uint16_t tmp = 0x7400;
                out << tmp << std::endl;
                tmp = get_absolute_dest();
                out << tmp << std::endl;
            }
        };

        class Test : public Base {

        public:
            virtual void write_asm(std::ostream & out) override {
                out << "test" << std::endl;
            }

            void write_bin(std::ostream &out) override {
                uint16_t tmp = 0x1600;
                out << tmp<< std::endl;
            }
        };

        class Move : public Base {
        public:
            virtual void write_asm(std::ostream & out) override {
                out << "move"  << std::endl;
            }

            void write_bin(std::ostream &out) override {
                uint16_t tmp = 0x0e00;
                out << tmp<< std::endl;
            }
        };

        class Read_FP : public Base {
        public:
            const int offset;
            Read_FP(int o) : offset(o) {}

            virtual void write_asm(std::ostream & out) override {
                out << "read\t"  << offset << "(FP)"  << std::endl;
            }

            void write_bin(std::ostream &out) override {
                uint16_t p = 0x0300;
                uint8_t tmp = offset;
                out << (p ^ tmp) << std::endl;
            }
        };

        class Read_GP : public Base {
        public:
            const int offset;
            Read_GP(int o) : offset(o) {}
            virtual void write_asm(std::ostream & out) override {
                out << "read\t" << offset << "(GP)"  << std::endl;
            }

            void write_bin(std::ostream &out) override {
                uint16_t p = 0x0200;
                uint16_t tmp = offset;
                out << (p ^ tmp) << std::endl;
            }
        };

    }

    class Block {
    private:
        int offset_record = 0;
        std::vector<Instruction::Base*> code;
        int parameter_no = 0;
        bool isRootBlock;
        Environment *global_env;
        Environment *local_env;
        Context     *context;
        void halt();
        void addition();
        void assign(const Assign_Statement*stat);
        void call(const char* funcname);
        void cond(const If_Statement*stat);
        void down();
        void eval(const Expression *exp);
        void execute(const Compound_Statement *seq);
        void execute(const Statement*stat);
        void leave(const Return_Statement*stat);
        void leave();
        void load(const Identifier* var);
        void load_gp(int offset);
        void load_fp(int offset);
        void read_gp(int offset);
        void read_fp(int offset);
        void store_gp(int offset);
        void store_fp(int offset);
        void loop(const While_Statement* stat);
        void move(const Move_Statement* stat);
        void read(const Identifier* id);
        void multiplication();
        void negate();
        void push_immediate(int value);
        void store_global(const Identifier *id);
        void subtract();
        void up();
        void allocate(int x);
        void pop(int n);
        void store(const Identifier* id);
        void eval(const Comparison * comp);
        Instruction::Rjump* conditional_jump(If_Operator op);
        Instruction::Rjump*  relative_jump();
        void insert(Instruction::Base* i) {
            offset_record += i->words();
            code.push_back(i);
        }
        void access(const Identifier* id,
                    void (Block::*gp)(int o),
                    void (Block::*fp)(int o),
                    const std::string& msg);
        int offset() { return offset_record; }
        Block *spawn(const Function_Declaration *decl);
        Block() {}
    public:
        void write(std::ostream  &out,
                   void (Instruction::Base::*writer)(std::ostream &out),
                   std::vector<Block*> *order = 0);
        std::vector<Block*>* relocate(); // relocate function entry point
        Block(const Program *prog);
    };

    std::vector<Block*>* Block::relocate() {
        if (!isRootBlock) {
            return 0;
        }

        auto blocks = new std::vector<Block*>();
        int offset = offset_record;
        for (auto  i : *context) {
            blocks->push_back(i.second);
            context->redecl(i.first, offset);
            offset += i.second->offset_record;
        }

        offset = 0;
        for (auto &i : code) {
            if (i->type() == Instruction::Type::Jump) {
                auto x = dynamic_cast<Instruction::Jump*>(i);
                x->set_absolute_dest(context->finddecl(x->entry));
            } else if (i->type() == Instruction::Type::Rjump) {
                auto x = dynamic_cast<Instruction::Rjump*>(i);
                x->set_absolute_dest(x->get() + offset);
            }
            offset += i->words();
        }

        for (auto b : *blocks) {
            for (auto &i : b->code) {
                    if (i->type() == Instruction::Type::Jump) {
                        auto x = dynamic_cast<Instruction::Jump*>(i);
                        x->set_absolute_dest(context->finddecl(x->entry));
                    } else if (i->type() == Instruction::Type::Rjump) {
                        auto x = dynamic_cast<Instruction::Rjump*>(i);
                        x->set_absolute_dest(x->get() + offset);
                    }
                    offset += i->words();
            }
        }
        return blocks;
    }

    void Block::halt() {
        insert(new Instruction::Halt());
    }

    void Block::store_gp(int o) {
        insert(new Instruction::Store_GP(o));
    }

    void Block::store_fp(int o) {
        insert(new Instruction::Store_FP(o));
    }

    void Block::read_gp(int o) {
        insert(new Instruction::Read_GP(o));
    }

    void Block::read_fp(int o) {
        insert(new Instruction::Read_FP(o));
    }

    void Block::move(const Move_Statement* stat) {
        eval(stat->left);
        eval(stat->right);
        insert(new Instruction::Move());
    }

    void Block::access(const Identifier* id,
                       void (Block::*gp)(int o),
                       void (Block::*fp)(int o),
                       const std::string& msg) {
        const char *var = id->name.c_str();
        int o = local_env->find(var);
        if (o == -1) {
            o = global_env->find(var);
            if (o == -1) {
                fatal(id->lineno, id->colno, msg+ " undefined variable " + id->name);
            } else {
                (this->*gp)(o);
            }
        } else {
            (this->*fp)(o);
        }
    }

    void Block::store_global(const Identifier *id) {
        const char *var = id->name.c_str();
        int o  = global_env->find(var);
        if (o == -1) {
            fatal(id->lineno, id->colno, "write to undefined variable " + id->name);
        } else {
            store_gp(o);
        }
    }

    void Block::load(const Identifier* id) {
        access(id, &Block::load_gp, &Block::load_fp, "refer to");
    }

    void Block::read(const Identifier* id) {
        access(id, &Block::read_gp, &Block::read_fp, "read");
    }

    void Block::store(const Identifier* id) {
        access(id, &Block::store_gp, &Block::store_fp, "write to");
    }

    void Block::eval(const Comparison * comp) {
        eval(comp->left);
        eval(comp->right);
        subtract();
        insert(new Instruction::Test());
        pop(1);
    }

    void Block::leave(const Return_Statement*stat) {
        if (isRootBlock) {
            fatal(stat->lineno, stat->colno, "return statement occur inside main program");
        } else {
            eval(stat->exp);
            store_fp(-2 - parameter_no);
            insert(new Instruction::Return());
        }
    }

    void Block::leave() {
        insert(new Instruction::Return());
    }

    Instruction::Rjump * Block::conditional_jump(If_Operator op) {
        if (op == OP_EQ) {
            return new Instruction::Jeq();
        } else if (op == OP_LT) {
            return new Instruction::Jlt();
        }
        return 0;
    }

    Instruction::Rjump* Block::relative_jump() {
        return new Instruction::Relative_Jump();
    }

    void Block::loop(const While_Statement* stat) {
// 0 comp -- o1
// 1 jeq  -- +2
// 2 jump -- o2 -> o4
// 4 while1
// 5 while2
// 6 jump -- o3 -> o1
// 7 end  -- o4
        int o1 = offset();
        eval(stat->cond);
        auto x = conditional_jump(stat->cond->op);
        x->set(4);
        insert(x);
        int o2 = offset();
        auto y = relative_jump();
        execute(stat->body);
        int o3 = offset();
        auto z = relative_jump();
        insert(z);
        int o4 = offset();
        y->set(o4 - o2);
        z->set(o3 - o1);
    }

    void Block::cond(const If_Statement*stat) {
        if (!stat->else_clause) { // part if
// 0 comp
// 1 jeq
// 2 jump -- o2 -> o3
// 3 if1
// 4 if2
// 5 end -- o3
            eval(stat->comp);
            auto x = conditional_jump(stat->comp->op);
            x->set(4); // FIXME, 2
            insert(x);
            int o2 = offset();
            auto y = relative_jump();
            insert(y);
            execute(stat->if_clause);
            int o3 = offset();
            y->set(o3 - o2);
        } else { // full if-else
// 0 test
// 1 jeq  -- o1
// 2 else1
// 3 else2
// 4 jump -- o2
// 5 if1
// 6 if2
// 7 end  -- o3
            eval(stat->comp);
            int o1 = offset();
            auto x = conditional_jump(stat->comp->op); // o1 -> (o2+1)
            insert(x);
            execute(stat->else_clause);
            int o2 = offset();
            auto y = relative_jump(); // o2 -> o3
            insert(y);
            execute(stat->if_clause);
            int o3 = offset();
            x->set(o2 + 1 - o1);
            y->set(o3 - o2);
        }
    }

    void Block::assign(const Assign_Statement *stat) {
        eval(stat->exp);
        store(stat->id);
    }

    void Block::push_immediate(int value) {
        insert(new Instruction::Immediate(value));
    }

    void Block::multiplication() {
        insert(new Instruction::Multiplication());
    }

    void Block::load_gp(int offset) {
        insert(new Instruction::Load_GP(offset));
    }

    void Block::load_fp(int offset) {
        insert(new Instruction::Load_FP(offset));
    }

    void Block::call(const char *name) {
        insert(new Instruction::Jump(name));
    }

    void Block::allocate(int x) {
        for (int i=0; i < x; i++) {
            load_gp(0);
        }
    }

    void Block::addition() {
        insert(new Instruction::Addition());
    }

    void Block::down() {
        insert(new Instruction::Down());
    }

    void Block::negate() {
        insert(new Instruction::Negation());
    }

    void Block::up() {
        insert(new Instruction::Up());
    }

    void Block::subtract() {
        insert(new Instruction::Subtraction());
    }

    void Block::pop(int n) {
        insert(new Instruction::Pop(n));
    }


    void Block::execute(const Statement* stat) {
        switch (stat->type()) {
        case Statement::Type::Up:
            up();
            info("up stat");
            break;
        case Statement::Type::Down:
            down();
            info("dsown stat");
            break;
        case Statement::Type::Call:
            eval(dynamic_cast<const Function_Call_Statement*>(stat)->exp);
            pop(1); // drop return value
            info("call stat");
            break;
        case Statement::Type::Move:
            move(dynamic_cast<const Move_Statement*>(stat));
            info("move stat");
            break;
        case Statement::Type::Read:
            read(dynamic_cast<const Read_Statement*>(stat)->id);
            info("read stat");
            break;
        case Statement::Type::Assign:
            assign(dynamic_cast<const Assign_Statement*>(stat));
            info("assign stat");
            break;
        case Statement::Type::If:
            cond(dynamic_cast<const If_Statement*>(stat));
            info("if stat");
            break;
        case Statement::Type::While:
            loop(dynamic_cast<const While_Statement*>(stat));
            info("while stat");
            break;
        case Statement::Type::Return:
            leave(dynamic_cast<const Return_Statement*>(stat));
            info("return stat");
            break;
        case Statement::Type::Compound:
            execute(dynamic_cast<const Compound_Statement*>(stat));
            info("compound stat");
            break;
        default:
            info("unknown stat");
            break;
        }
    }

    void Block::execute(const Compound_Statement* seq) {
        for (auto i = seq->cbegin(); i != seq->cend(); i++) {
            execute(*i);
        }
    }

    void Block::eval(const Expression *exp) {
        if (!exp) {
            push_immediate(0);
            return;
        }

        switch (exp->type()) {
        case Expression::Type::Funccall:
        {
            auto e = dynamic_cast<const Function_Call_Expression*>(exp);
            allocate(1); // for return value
            if (context->match(e)) {
                if (e->list) {
                    for (auto x = e->list->cbegin(); x != e->list->cend(); x++) {
                        const Expression *ee = *x;
                        eval(ee);
                    }
                }
            } else {
                fatal(e->id->lineno, e->id->colno, "function call " + e->id->name +
                      "() not match with declaration.");
                exit(3);
            }
            call(e->id->name.c_str());
            // remove arguments
            if (e->list) {
                pop(e->list->size());
            }
            info( "funcall exp");
            break;
        }
        case Expression::Type::Binary:
        {
            auto e = dynamic_cast<const Binary_Expression*>(exp);
            eval(e->left);
            eval(e->right);
            switch (e->op) {
            case OP_PLUS:
                info("bin exp +");
                addition();
                break;
            case OP_MINUS:
                info("bin exp -");
                subtract();
                break;
            case OP_TIMES:
                info("bin exp *");
                multiplication();
                break;
            default:
                info("unknown bin exp");
                break;
            }
            break;
        }
        case Expression::Type::Unary:
        {
            auto e = dynamic_cast<const Unary_Expression*>(exp);
            eval(e->exp);
            negate();
            info("negation exp");
            break;
        }
        case Expression::Type::Literal:
            push_immediate(dynamic_cast<const Number_Expression*>(exp)->val->value);
            info("number exp");
            break;
        case Expression::Type::Variable:
            load(dynamic_cast<const Identifier_Expression*>(exp)->id);
            info("variable exp");
            break;
        default:
            info("unknown exp");
            break;
        }
    }

    void Block::write(std::ostream  &out,
                      void (Instruction::Base::*writer)(std::ostream &out),
                      std::vector<Block*> *order) {
        // write body
        if (isRootBlock && !order) {
            out << "\t; main program" << std::endl;
        }

        for (auto i : code) {
            (i->*writer)(out);
        }

        // write function definitions
        if (isRootBlock) {
            if (order) {
                for (auto i : *order) {
                    for (auto c : i->code) {
                        (c->*writer)(out);
                    }
                }
            } else {
                for (auto i : *context) {
                    out << "\t; function: " << i.first << std::endl;
                    Block* m = i.second;
                    for (auto c : m->code) {
                        (c->*writer)(out);
                    }
                }
            }
        }
    }

    Block::Block(const Program *prog) {
        parameter_no = 0;
        isRootBlock = true;
        context  = new Context();
        global_env = new Environment();
        local_env = new Environment();

        // initalize function signatures
        if (prog->function_list) {
            auto funclist = prog->function_list;
            const Function_Declaration *decl;
            const Identifier *id;
            for (auto i = funclist->cbegin(); i != funclist->cend(); i++)  {
                decl = *i;
                id = decl->id;
                if (context->finddecl(id->name.c_str()) >= 0) {
                    fatal(id->lineno, id->colno, "function " + id->name + " redefined");
                } else {
                    context->declare(id->name.c_str(), decl->parameter_no());
                }
            }
        }

        if (prog->bindings) {
            int offset = 0; // local variable store from GP
            for (auto i = prog->bindings->cbegin(); i != prog->bindings->cend(); i++) {
                const Binding *b = *i;
                const Identifier *id = b->id;
                // declare variable
                if (!(global_env->add(id->name.c_str(), offset++))) {
                    fatal(id->lineno, id->colno, "variable " + id->name + " redefined");
                } else {
                    eval(b->exp);
                    store_global(id);
                }
            }
        }

        execute(prog->body);
        halt();

        if (prog->function_list) {
            auto funclist = prog->function_list;
            const Function_Declaration *decl;
            const Identifier *id;
            for (auto i = funclist->cbegin(); i != funclist->cend(); i++)  {
                decl = *i;
                id = decl->id;
                info("define function:" + id->name);
                context->define(id->name.c_str(), spawn(decl));
            }
        }
    }

    Block *Block::spawn(const Function_Declaration *decl) {
        // merge parameters and local environment
        Block *nb = new Block();
        nb->parameter_no = decl->parameters->size();
        nb->isRootBlock = false;
        nb->context = context;
        nb->global_env = global_env;
        nb->local_env = new Environment();

        int offset = -1 - nb->parameter_no;
        for (auto i = decl->parameters->cbegin();
             i != decl->parameters->cend();
             i++, offset++) {
            auto id = *i;
            info("para: " + id->name);
            if (!(nb->local_env->add(id->name.c_str(), offset))) {
                fatal(id->lineno, id->colno, "variable " + id->name + " redefined");
            }
        }

        if (decl->bindings) {
            offset = 1; // local variable store from FP+1
            for (auto i = decl->bindings->cbegin(); i != decl->bindings->cend(); i++) {
                const Binding *b = *i;
                const Identifier *id = b->id;
                // declare variable
                if (!(nb->local_env->add(id->name.c_str(), offset++))) {
                    fatal(id->lineno, id->colno, "variable " + id->name + " redefined");
                } else {
                    nb->eval(b->exp);
                }
            }
        }
        nb->execute(decl->compound_statement);
        nb->leave();
        return nb;
    }

    void generate(const Program * prog, const char flag) {
        Block block(prog);
        std::vector<Block*>* order;
        if (flag == 'a' || flag == 'b') {
            order = block.relocate();
        }
        if (flag == 'a') {
            block.write(std::cout, &Instruction::Base::write_asm, order);
        } else if (flag == 'A') {
            block.write(std::cout, &Instruction::Base::write_asm);
        } else if (flag == 'b') {
            block.write(std::cout, &Instruction::Base::write_bin, order);
        }
    }

} // end namespace Turtle
