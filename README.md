## compiling

./autogen.sh && ./configure

need c++11

## command line options

* -i input file, default to stdin
* -a  output assembly code
* -A  output unrelocated assembly code

output always goto stdout.
